package org.singularity.impl.loader;

import org.singularity.core.binding.AbstractFactory;
import org.singularity.core.binding.Instantiable;
import org.singularity.core.io.LineSequencer;
import org.singularity.core.loader.Game;
import org.singularity.core.loader.Loader;
import org.singularity.core.util.Chain;
import org.singularity.core.util.RemoteClassLoader;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: harrynoob
 */
public class LoaderImpl implements Loader {

    private static final String URL_BASE = "https://oldschool1.runescape.com/";
    private static final String CONFIG_URL = "https://oldschool1.runescape.com/jav_config.ws";
    private final AbstractFactory factory;
    @Instantiable
    public LoaderImpl(AbstractFactory factory) {
        this.factory = factory;
    }

    private Map<String, String> loadParams() {
        try (LineSequencer ls = new LineSequencer(new URL(CONFIG_URL).openStream())) {
            Chainer chainer = new Chainer();
            ls.forEach(chainer);
            return chainer.getResults();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Game load() {
        Map<String, String> params = loadParams();
        try {
            RemoteClassLoader rcl = new RemoteClassLoader(new URL(URL_BASE + params.get("initial_jar")));
            rcl.loadResources();
            Class<? extends Applet> appClazz = rcl.loadClass("client").asSubclass(Applet.class);
            Applet applet = appClazz.newInstance();
            URL documentBase = new URL(URL_BASE);
            URL codeBase = new URL(params.get("codebase"));
            applet.setStub(new Stub(applet, documentBase, codeBase, params));
            return factory.instantiate(Game.class, rcl, applet);
        } catch (MalformedURLException | ClassNotFoundException
                | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final class Chainer implements Chain<String> {
        private static final Pattern APPLET_PARAM_PATTERN = Pattern.compile("(.*?)=(\\S*)");
        private static final Pattern GAME_PARAM_PATTERN = Pattern.compile("param=(.*?)=(\\S*)");
        private final Map<String, String> paramMap;
        public Chainer() {
            this.paramMap = new HashMap<>();
        }

        @Override
        public void apply(String s) {
            Matcher appletMatcher = APPLET_PARAM_PATTERN.matcher(s);
            while(appletMatcher.find()) {
                String a = appletMatcher.group(1);
                if(a.intern() != "msg") {
                    paramMap.put(a, appletMatcher.group(2));
                }
            }
            Matcher gameMatcher = GAME_PARAM_PATTERN.matcher(s);
            while(gameMatcher.find()) {
                paramMap.put(gameMatcher.group(1), gameMatcher.group(2));
            }
        }

        public Map<String, String> getResults() {
            return paramMap;
        }
    }

    private static final class Stub implements AppletStub {
        private final Applet applet;
        private final URL documentBase;
        private final URL codeBase;
        private final Map<String, String> parameters;
        private Stub(Applet applet, URL documentBase, URL codeBase, Map<String, String> parameters) {
            this.applet = applet;
            this.documentBase = documentBase;
            this.codeBase = codeBase;
            this.parameters = parameters;
        }

        @Override
        public boolean isActive() {
            return applet.isActive();
        }

        @Override
        public URL getDocumentBase() {
            return documentBase;
        }

        @Override
        public URL getCodeBase() {
            return codeBase;
        }

        @Override
        public String getParameter(String s) {
            return parameters.get(s);
        }

        @Override
        public AppletContext getAppletContext() {
            return applet.getAppletContext();
        }

        @Override
        public void appletResize(int i, int i2) {
        }
    }
}
