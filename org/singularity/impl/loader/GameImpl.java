package org.singularity.impl.loader;

import org.singularity.core.binding.Instantiable;
import org.singularity.core.loader.Game;

import java.applet.Applet;

/**
 * Author: harrynoob
 */
public final class GameImpl implements Game {
    private final ClassLoader classLoader;
    private final Applet applet;
    @Instantiable
    public GameImpl(ClassLoader classLoader, Applet applet) {
        this.classLoader = classLoader;
        this.applet = applet;
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }

    @Override
    public Applet getApplet() {
        return applet;
    }
}
