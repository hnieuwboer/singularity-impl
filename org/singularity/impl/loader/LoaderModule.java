package org.singularity.impl.loader;

import org.singularity.core.binding.AbstractFactory;
import org.singularity.core.loader.Game;
import org.singularity.core.loader.Loader;
import org.singularity.core.util.Module;

/**
 * Author: harrynoob
 */
public class LoaderModule implements Module {
    @Override
    public void setupBindings(AbstractFactory baf) {
        baf.bind(Loader.class).to(LoaderImpl.class);
        baf.bind(Game.class).to(GameImpl.class);
    }
}
